import 'dotenv/config'
import * as joi from 'joi'

interface EnvVariables {
    PORT: number;
    PRODUCT_MICROSERVICE_HOST: string;
    PRODUCT_MICROSERVICE_PORT: number;
}

const envsSchema = joi.object({
    PORT: joi.number().required(),
    PRODUCT_MICROSERVICE_HOST: joi.string().required(),
    PRODUCT_MICROSERVICE_PORT: joi.number().required(),
})
.unknown(true);

const { error, value } = envsSchema.validate(process.env);

if(error) {
    throw new Error(`Config validation error: ${error.message}`);
}

const envVars: EnvVariables = value;

export const envs = {
    port: envVars.PORT,
    productsMicroServiceHost: envVars.PRODUCT_MICROSERVICE_HOST,
    productsMicroServicePort: envVars.PRODUCT_MICROSERVICE_PORT
}
