import { IsString, IsNumber, IsPositive, Min } from "class-validator";
import { Type } from "class-transformer"

export class CreateProductDto {

    @IsString()
    public name: string;

    @IsNumber({ maxDecimalPlaces: 4 })
    @IsPositive()
    @Min(0)
    @Type(() => Number)
    public price: number;
    
    @IsString()
    public sku: string;
}
