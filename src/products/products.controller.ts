import { 
  Controller, 
  Post, 
  Get, 
  Delete, 
  Param, 
  Inject, 
  Query, 
  ParseIntPipe, 
  Patch, 
  Body 
} from '@nestjs/common';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { PaginationDto } from 'src/common';
import { PRODUCT_SERVICE } from 'src/config';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';

@Controller('products')
export class ProductsController {
  constructor(
    @Inject(PRODUCT_SERVICE) private readonly productsClient: ClientProxy,
  ) { }

  @Post()
  async createProduct(@Body() createProductDto: CreateProductDto) {
    try {
      return await firstValueFrom(this.productsClient.send({ cmd: 'create_product' }, createProductDto))
    } catch (error) {
      throw new RpcException(error)
    }
  }

  @Patch(':id')
  async updateProduct(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProductDto: UpdateProductDto
  ) {
    try {
      return await firstValueFrom(this.productsClient.send({ cmd: 'update_product' }, { id, ...updateProductDto }))
    } catch (error) {
      throw new RpcException(error)
    }
  }

  @Get()
  findAllProducts(@Query() paginationDto: PaginationDto) {
    try {
      return this.productsClient.send({ cmd: 'find_all_products' }, paginationDto)
    } catch (error) {
      throw new RpcException(error)
    }
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    try {
      return await firstValueFrom(this.productsClient.send({ cmd: 'find_one_product' }, { id }))
    } catch (error) {
      throw new RpcException(error)
    }
  }

  @Delete(':id')
  async deleteProduct(@Param('id') id: string) {
    try {
      return await firstValueFrom(this.productsClient.send({ cmd: 'delete_product' }, { id }))
    } catch (error) {
      throw new RpcException(error)
    }
  }
}
